# this is a little script to help come up with a -DB_ID_REGEX
# if you are using non-Genbank formatted databases than odds are
# this regex that matches GENBANK deflines, 
#
#    [^\|]+(?:\|[^\|,\s]*){1,10}
#
# that matches GENBANK formatted deflines
# 
# sp|Q54152|XASA_SHIFL AMINO ACID ANTIPORTER...
#
# is not going to work for your database.  You may want to use something
# a bit more generic like,
#
#    [^ ]+
#
# which will match everything before the first space
# which would match H_DJ0592P03.Contig29 in this WashU defline
# 
# H_DJ0592P03.Contig29 : preliminary Length:47736
#
# This script will let you test your regex. 
# THE KEY to picking a good regex is to make sure your regex
# catches something which is UNIQUE to each sequence in the database.
# set $REGEX to the value of the regex you would like to test and
# set $BLAST_REPORT to a BLAST report against your formatted databases
# After you find a regex you will need to add -DB_ID_REGEX => YOUR_REGEX
# to your new declaration.
# 
# $b = new NHGRI::Blastall(-DB_ID_REGEX => '[^ ]+');
#

$REGEX = '[^ ]+';
$BLAST_REPORT = '../t/blast.out';

use NHGRI::Blastall;

$b = new NHGRI::Blastall( -DB_ID_REGEX => $REGEX );
$b->read_report( $BLAST_REPORT );
my @results = $b->result();

foreach $rh_r (@results) {
    print "id: $rh_r->{'id'}\n";
}



