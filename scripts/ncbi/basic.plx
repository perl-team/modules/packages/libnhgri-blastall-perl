#!/usr/local/bin/perl -w

use lib qw(/sysdev/users/jfryan/blastall.pm/NHGRI/Blastall);
use strict;
use NHGRI::Blastall;

my $b = new NHGRI::Blastall;

print "running ncbi blast...\n";

$b->blastall( p => 'blastn', 
              d => 'est', 
              e => 0.001,
              i => 'test.nt', 
              o => 'blastn.est.output' } );

$b->print_report();

