#!/usr/local/bin/perl -w

use lib qw(/sysdev/users/jfryan/blastall.pm/NHGRI/Blastall);
use strict;
use NHGRI::Blastall;

my $b = new NHGRI::Blastall;

print "running wu-blastn...\n";

$b->wu_blastall( {'p' => 'blastn', 
                  'd' => 'est', 
                  'i' => 'test.nt', 
                  'o' => 'blastn.est.output' } );

$b->print_report();

