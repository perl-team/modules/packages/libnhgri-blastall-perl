#!/usr/local/bin/perl -w

use strict;
use NHGRI::Blastall;

my $b = new NHGRI::Blastall;

print "running blastn over the network...\n";

$b->blastcl3( {'p' => 'blastn', 
               'd' => 'est', 
               'i' => 'test.nt', 
               'o' => 'blastn.est.output' } );

$b->print_report();


