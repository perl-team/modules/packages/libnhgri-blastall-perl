BEGIN { $| = 1; print "1..4\n"; }
END {print "not ok 1\n" unless $loaded;}
use NHGRI::Blastall;
$loaded = 1;
print "ok 1\n";

my $t = 't';

my $b = new NHGRI::Blastall;

if ($b->read_report("$t/blast.report")) {
    print "ok 2\n";
} else {
    print "not ok 2\n";
}

if ($b->result('id')) {
    print "ok 3\n";
} else {
    print "not ok 3\n";
}

if ($b->filter( {'identities' => .70} )) {
    print "ok 4\n";
} else {
    print "not ok 4\n";
}



